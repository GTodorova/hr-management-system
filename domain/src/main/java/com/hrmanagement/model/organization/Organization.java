
import java.time.LocalDateTime;

public class Organization {
    private int id;
    private String name;

    private String country;
    private FieldOfWork fieldOfWork;

    private LocalDateTime creationDate;

    public Organization() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public FieldOfWork getFieldOfWork() {
        return fieldOfWork;
    }

    public void setFieldOfWork(FieldOfWork fieldOfWork) {
        this.fieldOfWork = fieldOfWork;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }


}
