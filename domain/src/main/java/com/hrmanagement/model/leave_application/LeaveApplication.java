
import java.time.LocalDate;

public class LeaveApplication {

    private int id;

    private Leave_Type leaveType;
    private LocalDate startDate;

    private LocalDate endDate;

    private Leave_Status leaveStatus;

    public LeaveApplication() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Leave_Type getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(Leave_Type leaveType) {
        this.leaveType = leaveType;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Leave_Status getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(Leave_Status leaveStatus) {
        this.leaveStatus = leaveStatus;
    }
}
