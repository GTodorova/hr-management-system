package com.mymavenproject.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SpringbootAppApplicationTests.class)
class SpringbootAppApplicationTests {

	@Test
	void contextLoads() {
	}

}
